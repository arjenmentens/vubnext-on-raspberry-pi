# VUBnext on Raspberry Pi

A tutorial on how to connect your Raspberry Pi with the VUBnext or eduroam network @ Vrije Universiteit Brussel, Belgium.

## Description
Raspberry Pi has issues with handling username-password WiFi networks. This repository provides a quick solution which works on the Vrije Universiteit (VUB) campus Etterbeek with VUBnext and eduroam.

## Quickstart

- [Install Network Manager](https://gist.github.com/jjsanderson/ab2407ab5fd07feb2bc5e681b14a537a)
    - DO NOT edit `/etc/dhcpcd.conf` (this is done in the next step)
- Create a file, e.g. `wififix.sh` 

    `touch wififix.sh`
   
    and add

        #!/bin/bash
        sudo sed -i '1i\denyinterfaces wlan0' /etc/dhcpcd.conf
        sudo systemctl restart dhcpcd
        sleep 5
        sudo systemctl restart NetworkManager
        sleep 5
        sudo sed -i '1d' /etc/dhcpcd.conf
        sleep 5
        sudo systemctl restart dhcpcd`

- Make the file executable

    `chmod u+x wififix.sh`

- Run wififix at startup 

    `crontab -e` 

    and add

    `@reboot ./PATHTOFILE/wififix.sh`

## Visuals
Check [this screenshot](NetworkManager)

## Support
Contact [Arjen Mentens](mailto:Arjen.Mentens@vub.be) with an accurate description of your issue.

## Project status
Last update 18/12/2021. Will update if issues occur.

